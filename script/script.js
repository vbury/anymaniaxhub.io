var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerView: 1,
    paginationClickable: true,
    spaceBetween: 30,
    loop: true,
    autoplay: 3000,
});

var o = {
    init: function () {
        this.diagram();
    },
    random: function (l, u) {
        return Math.floor((Math.random() * (u - l + 1)) + l);
    },
    diagram: function () {
        var r = Raphael('diagram', '100%', '100%'),
            rad = 73;

        r.circle(300, 300, 85).attr({
            stroke: 'none',
            fill: '#193340'
        });

        var title = r.text(300, 300, 'Web').attr({
            font: '20px Arial',
            fill: '#fff'
        }).toFront();

        r.customAttributes.arc = function (value, color, rad) {
            var v = 3.6 * value,
                alpha = v == 360 ? 359.99 : v,
                random = o.random(91, 240),
                a = (random - alpha) * Math.PI / 180,
                b = random * Math.PI / 180,
                sx = 300 + rad * Math.cos(b),
                sy = 300 - rad * Math.sin(b),
                x = 300 + rad * Math.cos(a),
                y = 300 - rad * Math.sin(a),
                path = [['M', sx, sy], ['A', rad, rad, 0, +(alpha > 180), 1, x, y]];
            return {
                path: path,
                stroke: color
            };
        };

        $('.get').find('.arc').each(function (i) {
            var t = $(this),
                color = t.find('.color').val(),
                value = t.find('.percent').val(),
                text = t.find('.text').text();

            rad += 30;
            var z = r.path().attr({
                arc: [value, color, rad],
                'stroke-width': 26
            });

            z.mouseover(function () {
                this.animate({
                    'stroke-width': 50,
                    opacity: .75
                }, 1000, 'elastic');
                if (Raphael.type != 'VML') //solves IE problem
                    this.toFront();
                title.animate({
                    opacity: 0
                }, 500, '>', function () {
                    this.attr({
                        text: text + '\n' + value + '%'
                    }).animate({
                        opacity: 1
                    }, 500, '<');
                });
            }).mouseout(function () {
                this.animate({
                    'stroke-width': 26,
                    opacity: 1
                }, 1000, 'elastic');
            });
        });

        var e = Raphael('diagram2', '100%', '100%'),
            rad = 73;

        e.circle(300, 300, 85).attr({
            stroke: 'none',
            fill: '#193340'
        });

        var title2 = e.text(300, 300, 'Programmation').attr({
            font: '20px Arial',
            fill: '#fff'
        }).toFront();

        e.customAttributes.arc = function (value, color, rad) {
            var v = 3.6 * value,
                alpha = v == 360 ? 359.99 : v,
                random = o.random(91, 240),
                a = (random - alpha) * Math.PI / 180,
                b = random * Math.PI / 180,
                sx = 300 + rad * Math.cos(b),
                sy = 300 - rad * Math.sin(b),
                x = 300 + rad * Math.cos(a),
                y = 300 - rad * Math.sin(a),
                path = [['M', sx, sy], ['A', rad, rad, 0, +(alpha > 180), 1, x, y]];
            return {
                path: path,
                stroke: color
            };
        };

        $('.get2').find('.arc2').each(function (a) {
            var t = $(this),
                color = t.find('.color2').val(),
                value = t.find('.percent2').val(),
                text = t.find('.text2').text();

            rad += 30;
            var z = e.path().attr({
                arc: [value, color, rad],
                'stroke-width': 26
            });

            z.mouseover(function () {
                this.animate({
                    'stroke-width': 50,
                    opacity: .75
                }, 1000, 'elastic');
                if (Raphael.type != 'VML') //solves IE problem
                    this.toFront();
                title2.animate({
                    opacity: 0
                }, 500, '>', function () {
                    this.attr({
                        text: text + '\n' + value + '%'
                    }).animate({
                        opacity: 1
                    }, 500, '<');
                });
            }).mouseout(function () {
                this.animate({
                    'stroke-width': 26,
                    opacity: 1
                }, 1000, 'elastic');
            });
        });
    }

};

$(".jq").mouseover(function () {
    $('.jq').stop().animate({
        width: 139,
        opacity: .75
    }, 500).text("JavaScript : 75%")
}).stop().mouseout(function () {
    $('.jq').animate({
        width: 85,
        opacity: 1,
    }, 500).text("JavaScript")
});

$(".css").mouseover(function () {
    $('.css').stop().animate({
        width: 100,
        opacity: .75
    }, 500).text("CSS3 : 85%")
}).stop().mouseout(function () {
    $('.css').animate({
        width: 46,
        opacity: 1,
    }, 500).text("CSS3")
});

$(".html").mouseover(function () {
    $('.html').stop().animate({
        width: 103,
        opacity: .75
    }, 500).text("HTML : 90%")
}).stop().mouseout(function () {
    $('.html').animate({
        width: 59,
        opacity: 1,
    }, 500).text("HTML5")
});

$(".node").mouseover(function () {
    $('.node').stop().animate({
        width: 118,
        opacity: .75
    }, 500).text("NodeJS : 70%")
}).stop().mouseout(function () {
    $('.node').animate({
        width: 65,
        opacity: 1,
    }, 500).text("NodeJS")
});

$(".java").mouseover(function () {
    $('.java').stop().animate({
        width: 91,
        opacity: .75
    }, 500).text("Java : 60%")
}).stop().mouseout(function () {
    $('.java').animate({
        width: 38,
        opacity: 1,
    }, 500).text("Java")
});

$(".c1").mouseover(function () {
    $('.c1').stop().animate({
        width: 77,
        opacity: .75
    }, 500).text("C# : 65%")
}).stop().mouseout(function () {
    $('.c1').animate({
        width: 23,
        opacity: 1,
    }, 500).text("C#")
});

$(".c").mouseover(function () {
    $('.c').stop().animate({
        width: 67,
        opacity: .75
    }, 500).text("C : 55%")
}).stop().mouseout(function () {
    $('.c').animate({
        width: 13,
        opacity: 1,
    }, 500).text("C")
});

$(".sql").mouseover(function () {
    $('.sql').stop().animate({
        width: 102,
        opacity: .75
    }, 500).text("Mysql : 70%")
}).stop().mouseout(function () {
    $('.sql').animate({
        width: 48,
        opacity: 1,
    }, 500).text("Mysql")
});

$('#arrow').mouseover(function () {
    $('#arrow2').attr('src', './images/arrows4.png')
}).mouseout(function () {
    $('#arrow2').attr('src', './images/arrows3.png')
});

$('#arrow').click(function () {
    $('html, body').animate({
        scrollTop: $("#cv").offset().top
    }, 2000);
});

$('#cv2').click(function () {
    $('html, body').animate({
        scrollTop: $("#cv").offset().top
    }, 2000);
});

function getBrowserSize() {
    var size = new Array();
    if (typeof (window.innerWidth) == 'number') {
        size[0] = window.innerWidth;
        size[1] = window.innerHeight;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        size[0] = document.documentElement.clientWidth;
        size[1] = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        size[0] = document.body.clientWidth;
        size[1] = document.body.clientHeight;
    } else {
        size[0] = -1;
        size[1] = -1;
    }
    return size;
}